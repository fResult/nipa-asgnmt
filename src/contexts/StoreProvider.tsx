import React, { createContext, FC, ReactNode, useState } from 'react'
import { StoreContextType } from 'types/context'

type StoreProviderProps = {
  children: ReactNode
}

export const StoreContext = createContext<StoreContextType | null>(null)

const StoreProvider: FC<StoreProviderProps> = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false)
  return (
    <StoreContext.Provider value={{ isLoading, setIsLoading }}>
      {children}
    </StoreContext.Provider>
  )
}

export default StoreProvider
