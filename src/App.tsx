import './App.pcss'
import React, { lazy, Suspense } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
import Home from 'pages/Home'

export type DetectObjectsPathType = 'by-webcam' | 'by-image'

const routeConfigs = [
  { path: '/', component: Home },
  {
    path: '/detect-objects/:method',
    component: lazy(() => import('pages/DetectObjects'))
  }
  // { path: '/test', component: lazy(() => import('pages/Test')) }
]

function App(): JSX.Element {
  return (
    <div className="App">
      <Suspense fallback={<h1>Loading...</h1>}>
        <Router>
          <Switch>
            {routeConfigs.map(({ path, component }) => {
              return <Route {...{ key: path, path, component }} exact />
            })}
            <Route component={() => <Redirect to="/" />} />
          </Switch>
        </Router>
      </Suspense>
    </div>
  )
}

export default App
