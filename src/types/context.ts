import { Dispatch, SetStateAction } from 'react'

export type StoreContextType = {
  isLoading: boolean
  // setIsLoading: Dispatch<SetStateAction<boolean>>
  setIsLoading: (isLoading: boolean) => void
}
