export type ProcessEnv = {
  [key: string]: string | undefined
}
