import {
  NvisionRequest as OldNvsionRequest,
  ObjectDetectionResult
} from '@nipacloud/nvision/dist/models/NvisionRequest'
import { ObjectDetectionParameter } from '@nipacloud/nvision/dist/services/ObjectDetectionService'

type NvisionRequest = Omit<OldNvsionRequest, 'detected_object'> & {
  detected_objects?: ObjectDetectionResult[]
}
export type { ObjectDetectionParameter, NvisionRequest, ObjectDetectionResult }
