import './Footer.pcss'
import React, { CSSProperties, FC, ReactNode } from 'react'
import { BottomNavigation } from '@material-ui/core'

type FooterProps = {
  className?: string
  children: ReactNode
  style?: CSSProperties
}
const Footer: FC<FooterProps> = ({ style, children, className = '' }) => {
  return (
    <BottomNavigation className={`layout-footer ${className}`} style={style}>
      JUST FOOTER
    </BottomNavigation>
  )
}

export default Footer
