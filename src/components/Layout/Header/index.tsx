import './Header.pcss'
import React, { CSSProperties, FC } from 'react'

import {
  AppBar,
  createStyles,
  Grid,
  Link,
  makeStyles,
  Theme
} from '@material-ui/core'

import { Link as RouterLink } from 'react-router-dom'

type HeaderProps = {
  className?: string
  style?: CSSProperties
}

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary
    }
  })
})

const Header: FC<HeaderProps> = ({ className = '', style }) => {
  const classes = useStyles()
  return (
    <AppBar
      className={`layout-header flex items-center justify-between px-6 xs:px-8 sm:px-10 ${className}`}
      position="static"
      style={{ height: 64, backgroundColor: '#E91D76', ...style }}
    >
      <Grid
        container
        className={`justify-end md:justify-between ${classes.root}`}
      >
        <Grid
          item
          sm={8}
          className="hidden md:flex justify-start items-center font-extrabold"
        >
          Nipa Cloud - Object Detection Utilized Application
        </Grid>
        <Grid item container sm={4} className="flex justify-end items-center">
          <>
            <span className="text-gray-200 font-bold">Detect by:&nbsp;</span>
            <nav>
              <Link
                className="link"
                component={RouterLink}
                underline="always"
                color="initial"
                to="/detect-objects/by-webcam"
              >
                Webcam
              </Link>
              &nbsp;|&nbsp;
              <Link
                className="link"
                component={RouterLink}
                underline="always"
                color="initial"
                to="/detect-objects/by-image"
              >
                Browse image
              </Link>
            </nav>
          </>
        </Grid>
      </Grid>
    </AppBar>
  )
}

export default Header
