import './Content.pcss'
import React, { CSSProperties, FC, ReactNode } from 'react'

type ContentProps = {
  className?: string
  children: ReactNode
  style?: CSSProperties
}
const Content: FC<ContentProps> = ({ style, children, className = '' }) => {
  return (
    <section
      className={`layout-content py-8 px-6 xs:px-8 sm:px-10 ${className}`}
      style={style}
    >
      {children}
    </section>
  )
}

export default Content
