import React, { CSSProperties, FC, HTMLAttributes, ReactNode } from 'react'

import Header from './Header'
import Footer from './Footer'
import Content from './Content'

type LayoutProps = {
  children: ReactNode
  name: string
  className?: string
  style?: CSSProperties
}

const Layout: FC<HTMLAttributes<HTMLDivElement> & LayoutProps> = ({
  name,
  className = '',
  children,
  style,
  ...props
}) => {
  const { onKeyDown } = props
  return (
    <div
      className={`layout page-${name} ${className}`}
      {...{ style, onKeyDown }}
    >
      {children}
    </div>
  )
}

export { Header, Content, Footer, Layout }
