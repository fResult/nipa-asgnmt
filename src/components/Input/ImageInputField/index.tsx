import './ImageInputField.pcss'
import React, { FC, useEffect } from 'react'
import { Button, CardMedia, CircularProgress, Link } from '@material-ui/core'
import { DropzoneArea } from 'material-ui-dropzone'
import { CancelRounded } from '@material-ui/icons'

type ImageInputFieldProps = {
  hasFile: boolean
  onChangeImage: (files: File[]) => void
  fileSize: number
  fileName: string
  onClearResult: () => void
  loading: boolean
  imageSrc?: string
}

const ImageInput: FC<ImageInputFieldProps> = ({
  hasFile,
  onChangeImage,
  fileSize,
  fileName,
  onClearResult,
  loading,
  imageSrc = ''
}) => {
  useEffect(() => {
    return onClearResult /* when component is unmounting */
  }, [])

  return (
    <div className="upload-container bg-white w-full h-auto sm:w-96 sm:h-72 mx-auto mb-4 overflow-hidden rounded-md">
      <div className="bg-black h-full bg-opacity-05">
        {!hasFile ? (
          <DropzoneArea
            dropzoneClass="h-full w-full max-w-96 input-upload"
            acceptedFiles={['image/*' /*,'video', 'application'*/]}
            onChange={onChangeImage}
            filesLimit={1}
            dropzoneText={`Drop file here or Browse`}
            showPreviews={false}
          />
        ) : (
          <div className="image-container w-full h-full">
            <div className="image-header text-xs w-full h-12 flex justify-between items-center px-2">
              <div className="text-white flex flex-col items-start">
                <span className="text-sm opacity-80">{fileName}</span>
                <span className="opacity-60">
                  {Math.round(fileSize / 1024)} KB
                </span>
              </div>
              <div className="flex text-white gap-2">
                <span>{loading ? 'Uploading...' : 'Upload completed'}</span>
                <Button
                  size="small"
                  variant="contained"
                  className="btn-close"
                  onClick={onClearResult}
                  disabled={loading}
                >
                  {loading ? (
                    <CircularProgress
                      style={{ color: '#FFF', height: 'auto' }}
                    />
                  ) : (
                    <CancelRounded className="opacity-80" />
                  )}
                </Button>
              </div>
            </div>
            <div className="image-wrap flex">
              <CardMedia
                className="image cursor-pointer"
                component="img"
                src={imageSrc}
                alt="Uploaded image"
                onClick={() => {
                  const image = new Image()
                  image.src = imageSrc
                  const w = window.open('')!
                  w.document.write(image.outerHTML)
                }}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default ImageInput
