import './WebcamInputField.pcss'
import React, { FC, forwardRef, MouseEvent, useEffect } from 'react'
import Webcam from 'react-webcam'
import { Button, CardMedia } from '@material-ui/core'
import { CameraAlt } from '@material-ui/icons'

type WebcamInputFieldProps = {
  hasFile: boolean
  onCapture?: (e: MouseEvent<HTMLButtonElement>) => void
  onClearResult: () => void
  fileName?: string
  fileSize?: number
  imageSrc?: string
}

const videoConstraints = {
  width: 260,
  height: 200,
  facingMode: 'user'
}

const WebcamInputField: FC<WebcamInputFieldProps> = forwardRef<
  Webcam,
  WebcamInputFieldProps
>((props, ref) => {
  const {
    fileName,
    fileSize,
    onCapture,
    onClearResult,
    hasFile,
    imageSrc = ''
  } = props

  useEffect(() => {
    return onClearResult /* when component is unmounting */
  }, [])

  const arr: Array<string> = ['a', 'b', 'c', 'apple', 'orange']
  // const result = arr.reduce((acc: string[], char, idx) => {

  //   // acc [i] > now
  //   for (let i = 0; i<idx; i++) {
  //   }
  //   return idx === 0
  //     ? [char]
  //     : char > acc[idx - 1]
  //     ? [...acc, char]
  //     : [char, ...acc]
  // }, [])
  // console.log('result', result)

  return (
    <div className="input-webcam mb-3">
      <div className="webcam-wrap m-auto w-full sm:w-3/4 lg:w-1/2">
        {!hasFile ? (
          <Webcam
            className="m-auto w-full"
            audio={false}
            ref={ref}
            mirrored
            screenshotFormat="image/jpeg"
            videoConstraints={videoConstraints}
          />
        ) : (
          <figure>
            <figcaption>
              {`File Name: ${fileName}, File Size: ${fileSize} KB`}
            </figcaption>
            <CardMedia
              className="image cursor-pointer"
              component="img"
              src={imageSrc}
              alt="Captured image"
              onClick={() => {
                const image = new Image()
                image.src = imageSrc
                const w = window.open('')!
                w.document.write(image.outerHTML)
              }}
            />
          </figure>
        )}
        <Button
          className="btn-capture w-full focus:outline-none"
          onClick={onCapture}
          disabled={hasFile}
        >
          Capture <CameraAlt className="ml-1" />
        </Button>
      </div>
    </div>
  )
})

export default WebcamInputField
