import './Home.pcss'
import React, { Context, FC, useContext } from 'react'
import { Grid, Link, Typography } from '@material-ui/core'
import { Link as RouterLink } from 'react-router-dom'

import { Content, Header, Layout } from 'components/Layout'

import { StoreContext } from 'contexts/StoreProvider'

import { StoreContextType } from 'types/context'

const Home: FC = () => {
  // const { isLoading, setIsLoading } = useContext(
  //   StoreContext as Context<StoreContextType>
  // )

  return (
    <Layout name="home">
      <Header />
      <Content className="content-home flex flex-col">
        <Typography variant="h1" className="content-header" gutterBottom>
          Home Page
        </Typography>

        <nav className="text-center m-auto">
          <span className="font-bold">Detect by:&nbsp;</span>
          <span className="text-blue-500">
            <Link
              className="link"
              component={RouterLink}
              underline="always"
              color="initial"
              to="/detect-objects/by-webcam"
            >
              Webcam
            </Link>
            &nbsp;|&nbsp;
            <Link
              className="link"
              component={RouterLink}
              underline="always"
              color="initial"
              to="/detect-objects/by-image"
            >
              Browse image
            </Link>
          </span>
        </nav>
      </Content>
    </Layout>
  )
}

export default Home
