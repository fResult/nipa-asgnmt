import React, {
  ChangeEvent,
  MouseEvent,
  CSSProperties,
  DOMAttributes,
  FC,
  ReactNode,
  useRef,
  useState,
  useEffect
} from 'react'

const Test = () => {
  const [file, setFile] = useState<File | null>(null)

  const fileRef = useRef<HTMLInputElement | null>(null)
  const textareaRef = useRef<HTMLTextAreaElement | null>(null)

  useEffect(() => {
    const textarea = textareaRef.current
    textarea?.focus()
  })

  function handleChangeFileState(e: ChangeEvent<HTMLInputElement>) {
    setFile(e.target.files?.[0] as File)
  }

  function handleUploadFileState() {
    console.log('UploadFileState', file)
  }

  function handleUploadFileRef() {
    const file = fileRef.current!.files?.[0]
    console.log('UploadFileRef', file)
  }

  function handleSelectAll() {
    console.log('handleSelectAll', buttonRef)
    textareaRef.current!.focus()
    textareaRef.current!.select()
  }

  const buttonRef = useRef<HTMLButtonElement | null>(null)

  return (
    <div className="page-test container h-screen flex flex-col items-center justify-center m-auto">
      <fieldset className="border w-full p-3 text-left border-black">
        <legend>STATE</legend>
        <input
          type="file"
          className="w-full border p-4 mb-3"
          onChange={handleChangeFileState}
        />
        <button
          className="p-4 w-full focus:outline-none border bg-gray-50 hover:bg-gray-100"
          onClick={handleUploadFileState}
          disabled={!file}
        >
          อัพโหลดไฟล์
        </button>
      </fieldset>

      <fieldset className="border w-full p-3 text-left my-8 border-black">
        <legend>Ref & ForwardRef</legend>
        <input type="file" ref={fileRef} className="w-full border p-4 mb-3" />
        <button
          className="p-4 w-full focus:outline-none border bg-gray-50 hover:bg-gray-100"
          onClick={handleUploadFileRef}
        >
          อัพโหลดไฟล์
        </button>

        <textarea
          ref={textareaRef}
          className="w-full border focus:outline-none p-2 my-3"
          rows={5}
        />
        <Button onClick={handleSelectAll}>เลือกทั้งหมด</Button>
        <ButtonWithForwardRef ref={buttonRef} onClick={handleSelectAll}>
          เลือกทั้งหมด (ForwardRef)
        </ButtonWithForwardRef>
      </fieldset>

      <fieldset className="border w-full p-3 text-left border-black">
        <legend>Remote to ref</legend>
        <Button onClick={() => buttonRef.current?.click()}>
          Remote select all to Textarea above
        </Button>
      </fieldset>
    </div>
  )
}

type ButtonProps = {
  className?: string
  style?: CSSProperties
}
const Button: FC<DOMAttributes<HTMLButtonElement> & ButtonProps> = (props) => {
  const { className, style } = props
  return (
    <button
      {...{ ...props, style }}
      className={`${className} btn p-4 w-full focus:outline-none border bg-gray-50 hover:bg-gray-200 focus:bg-gray-200 button-hover`}
    >
      {props.children}
    </button>
  )
}

type ButtonWithForwardRefProps = {
  onClick?: (e: MouseEvent<HTMLButtonElement>) => void

  children: ReactNode
} & ButtonProps
const ButtonWithForwardRef = React.forwardRef<
  HTMLButtonElement,
  ButtonWithForwardRefProps
>((props, ref) => {
  const { className = '', style, children, onClick = () => {} } = props
  return (
    <button
      {...{ ref, style, onClick }}
      className={`${className} btn p-4 w-full focus:outline-none border bg-gray-50 hover:bg-gray-200 focus:bg-gray-200 button-hover`}
    >
      {children}
    </button>
  )
})

export default Test
