import './DetectObjects.pcss'
import React, {
  Context,
  FC,
  MouseEvent,
  useCallback,
  useContext,
  useRef,
  useState
} from 'react'
import { useParams } from 'react-router'

import Webcam from 'react-webcam'
import { Content, Header, Layout } from 'components/Layout'
import WebcamInputField from 'components/Input/WebcamInputField'
import ImageInputField from 'components/Input/ImageInputField'
import { StoreContext } from 'contexts/StoreProvider'
import { StoreContextType } from 'types/context'
import { DetectObjectsPathType } from 'src/App'

import { objectDetection } from '@nipacloud/nvision'
import type {
  NvisionRequest,
  ObjectDetectionParameter,
  ObjectDetectionResult
} from 'types/nvision'
import {
  convertBase64ToKiloByte,
  convertFileToBase64
} from 'services/converter'

import { TextareaAutosize, Typography } from '@material-ui/core'

type DetectObjectsProps = {}

const objectDetectionService = objectDetection({
  apiKey: import.meta.env.VITE_API_KEY_OD
})

const DetectObjects: FC<DetectObjectsProps> = ({}) => {
  const params = useParams<{ method: DetectObjectsPathType }>()
  const webcamInputRef = useRef<Webcam | null>(null)
  const { isLoading, setIsLoading } = useContext(
    StoreContext as Context<StoreContextType>
  )
  const isWebcam = params.method === 'by-webcam'

  const isEnabledWebcam = useState(false)
  const [fileDesc, setFileDesc] = useState<
    Partial<{ name: string; size: number }>
  >({})
  const [detectedObjects, setDetectedObjects] = useState<
    ObjectDetectionResult[]
  >([])
  const [currentHeadBase64, setCurrentHeadBase64] = useState('')
  const [currentTailBase64, setCurrentTailBase64] = useState('')
  const [respTailBase64, setRespTailBase64] = useState('')

  const hasFile = respTailBase64 || currentTailBase64 || detectedObjects.length
  const jsonResult = !detectedObjects.length
    ? ''
    : detectedObjects.map((object: Partial<ObjectDetectionResult>) => {
        delete object.cropped_image
        return object
      })

  async function handleChangeImage(files: Array<File>): Promise<void> {
    if (files.length) {
      const { name, size } = files[0]
      setFileDesc({ name, size })
      const base64 = (await convertFileToBase64(files[0])) as string
      const parts = base64.split(',')
      setCurrentHeadBase64(parts[0])
      setCurrentTailBase64(parts[1])
      await detectObjects(parts[1])
    }
  }

  async function detectObjects(base64: string): Promise<void> {
    setIsLoading(true)
    const predictParams: ObjectDetectionParameter = {
      rawData: base64,
      outputCroppedImage: true,
      outputVisualizedImage: true
    }

    const {
      detected_objects = [],
      raw_data = '',
      service_id
    }: NvisionRequest = await objectDetectionService.predict(predictParams)

    if (!service_id) {
      alert('❌ Error occurred during upload.')
      return
    }
    setDetectedObjects(detected_objects)
    setRespTailBase64(raw_data as string)
    setIsLoading(false)
  }

  function clearImageStates(): void {
    setCurrentHeadBase64('')
    setCurrentTailBase64('')
    setRespTailBase64('')
    setDetectedObjects([])
  }

  function handleCloseImage(): void {
    clearImageStates()
  }

  const handleCapture: (
    e?: MouseEvent<HTMLButtonElement>
  ) => Promise<void> = useCallback(
    async (e) => {
      // e.preventDefault()
      const base64Image: string = webcamInputRef?.current?.getScreenshot()!

      const fileSize = convertBase64ToKiloByte(base64Image)
      setFileDesc({ name: 'Untitled.jpg', size: fileSize })

      const parts = base64Image.split(',')
      setCurrentHeadBase64(parts[0])
      setCurrentTailBase64(parts[1])
      await detectObjects(parts[1])
    },
    [webcamInputRef]
  )

  return (
    <Layout name="detect-objects">
      <Header />
      <Content className="content-detect-objects w-full">
        <div className="container m-auto">
          <Typography variant="h1" className="content-header" gutterBottom>
            DETECT OBJECTS PAGE -{' '}
            {isWebcam ? 'by webcam capturing' : 'by image uploading'}
          </Typography>
          {isWebcam ? (
            <div>
              <WebcamInputField
                {...{
                  fileName: fileDesc.name,
                  fileSize: fileDesc.size,
                  hasFile: !!hasFile,
                  imageSrc: [
                    currentHeadBase64,
                    respTailBase64 || currentTailBase64
                  ].join(','),
                  ref: webcamInputRef,
                  onCapture: handleCapture,
                  onClearResult: handleCloseImage
                }}
              />
            </div>
          ) : (
            <ImageInputField
              imageSrc={[
                currentHeadBase64,
                respTailBase64 || currentTailBase64
              ].join(',')}
              hasFile={!!hasFile}
              onChangeImage={handleChangeImage}
              onClearResult={handleCloseImage}
              fileSize={fileDesc.size!}
              fileName={fileDesc.name!}
              loading={isLoading}
            />
          )}

          {detectedObjects.length ? (
            <TextareaAutosize
              className="json-result"
              disabled
              value={JSON.stringify(jsonResult, null, 3)}
            />
          ) : null}
        </div>
      </Content>
    </Layout>
  )
}

export default DetectObjects
