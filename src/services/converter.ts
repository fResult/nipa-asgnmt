export function convertFileToBase64(file: File): Promise<string | void> {
  const fResult = new Promise<string>((resolve, reject) => {
    const fileReader = new FileReader()
    fileReader.readAsDataURL(file as Blob)
    fileReader.onload = () => {
      resolve(fileReader.result as string)
    }

    fileReader.onerror = (error) => {
      console.error('❌ Error: ', error)
      reject(error)
    }
  })

  return fResult
}

/**
 * **Reference formula:** https://softwareengineering.stackexchange.com/questions/288670#368406
 * @param { string } fullBase64 - base64 from image
 * @return { number } - KB
 */
export function convertBase64ToKiloByte(fullBase64: string): number {
  const bytes = fullBase64.length * (3 / 4)
  return Math.floor((bytes - 1) / 1024) // KBytes
}
