# 🌥🕵️‍ Nipa Cloud Assignment ️🕵️‍🌥
Using Object Detection integration with frontend.  

[comment]: <> ([![Nvision - Object Detection logo][OD_LOGO]][OD_WEB_PAGE])
<a href="https://nvision.nipa.cloud/ObjectDetection" target="_blank" rel="noreferrer">
  <img src="./src/assets/icons/favicon.png" width="150" alt="Nvision - Object Detection logo" />
</a>

##  🖼 Demo GIF images 🏃‍
![Detect by image uploading](https://gitlab.com/fResult/nipa-asgnmt/uploads/0a0aa036e67c4ac50700c2407977272c/by-image.gif)  
  
![Detect by webcam capturing](https://gitlab.com/fResult/nipa-asgnmt/uploads/7ed99f70f98cf3d3985ab2caecb2f4f0/by-webcam.gif)

### Features
- Detect objects by **drag & drop image**
- Detect objects by **capture from webcam**


### 🏃‍ How to set up and run 🏃‍
#### Setup
1. Create file `.env` at root directory.
2. Add variable `VITE_API_KEY_OD` follow below...
```dotenv
VITE_API_KEY_OD=<YOUR_API_KEY>
```

#### First time (Install & start):
```
npm run dev:init
#or
yarn dev:init
```
#### Second or more time:
```
npm run dev
#or
yarn dev
```

### 🛠 Built by 💻 ...
#### ⚙ Tools:
- [ViteJS](https://vitejs.dev)
- [React](https://reactjs.org)
- [React Router](https://reactrouter.com)
- [Material UI](https://material-ui.com)
  - [Material UI Dropzone](https://yuvaleros.github.io/material-ui-dropzone)
- [PostCSS](https://postcss.org)
  - [Tailwind CSS](https://tailwindcss.com)
- [Nvision - Object Detecting][OD_WEB_PAGE]

#### 🗨 Languages:
- HTML
- CSS
- Typescript

[OD_LOGO]: ./src/assets/icons/favicon.png
[OD_WEB_PAGE]: https://nvision.nipa.cloud/ObjectDetection
